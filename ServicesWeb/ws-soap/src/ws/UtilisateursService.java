package ws;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import modele.DBConnection;
import metier.Login;
import metier.Utilisateur;
@WebService(serviceName="UtilisateursService")
public class UtilisateursService {
	@WebMethod
	public String logIn(@WebParam(name="pseudo")String pseudo,@WebParam(name="password")String password) {
		try
	    {  
			Connection	conn = DBConnection.createConnection();
			String sql = "SELECT pseudo,password,typeUtilisateur FROM Utilisateur WHERE pseudo = ? AND password = ? ";
	        PreparedStatement pstmt = conn.prepareStatement(sql);
	        {    
	            // set parameters for statement
	            pstmt.setString(1,pseudo);
	            pstmt.setString(2,password);
	            ResultSet rs = pstmt.executeQuery();
	            if(rs.next()) {
	            	if(rs.getString(3).equals("admin"))
	            	return Login.createToken(pseudo, password);
	            	return "erreur";
	            }
	        }
	     }catch (SQLException ex) {
	    	 	
	            System.out.println(ex.getMessage());
	      }
	 return "erreur";
	}
	
	@WebMethod
	public List<Utilisateur>  listerUtilisateurs(@WebParam(name="token")String token,@WebParam(name="status")String status) {
		if(!Utilisateur.canAcces(token, status)) return null;
		List<Utilisateur> users = new ArrayList<Utilisateur>();
	ResultSet rs = null;
		try
	    {  
			Connection	conn = DBConnection.createConnection();
			Statement st = conn.createStatement();
			 rs=st.executeQuery("select * from Utilisateur");  
	      while(rs.next()) {
	      //System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getString(3)+" "+rs.getString(4)+" "+rs.getString(6)+" "+rs.getString(7)+" "+ rs.getString(8)); 
	              Utilisateur user = new Utilisateur();
	              user.setId(rs.getInt("id"));
	              user.setPrenom(rs.getString("prenom"));
	              user.setNom(rs.getString("nom"));
	              user.setPseudo(rs.getString("pseudo"));
	          //    user.setPassword(rs.getString("password"));
	              user.setTypeUtilisateur(rs.getString("typeUtilisateur"));
	              user.setDateDeCreation(rs.getDate(7));
	              user.setDateDeModification(rs.getDate(8));
	              users.add(user);
	          }
	    	  conn.close();
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	    }
		return users;
	}
	@WebMethod
	public String ajouterUtilisateur(@WebParam(name="prenom")String prenom,@WebParam(name="nom")String nom,@WebParam(name="pseudo")String pseudo,@WebParam(name="password")String password,@WebParam(name="typeUtilisateur")String typeUtilisateur,@WebParam(name="token")String token,@WebParam(name="status")String status) {
		if(!Utilisateur.canAcces(token, status)) return "access denied";
		try
	    {  
			Connection	conn = DBConnection.createConnection();
			String sql = "INSERT INTO Utilisateur(prenom,nom,pseudo,password,typeUtilisateur) "
	                   + "VALUES(?,?,?,?,?)";
	        PreparedStatement pstmt = conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
	        {    
	            // set parameters for statement
	            pstmt.setString(1,prenom);
	            pstmt.setString(2,nom);
	            pstmt.setString(3,pseudo);
	            pstmt.setString(4,password);
	            pstmt.setString(5,typeUtilisateur);
	            pstmt.executeUpdate();
	            }
	        }catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	      }
		return "L'utilisateur est bien ajoute";
	}
	@WebMethod
	public String modifierUtilisateur(@WebParam(name="token")String token,@WebParam(name="status")String status,@WebParam(name="id")int id,@WebParam(name="prenom")String prenom,@WebParam(name="nom")String nom,@WebParam(name="pseudo")String pseudo,@WebParam(name="password")String password,@WebParam(name="typeUtilisateur")String typeUtilisateur) {
		if(!Utilisateur.canAcces(token, status)) return "access denied";
		if(Utilisateur.userExist(id)) {
			try
		    {  
				Connection	conn = DBConnection.createConnection();
				String sql = "UPDATE Utilisateur "
		                + "SET prenom = ? ,"+" nom = ? ,"+" pseudo = ? ,"+" password = ? ,"+" typeUtilisateur = ? "
		                + "WHERE id = ?";
		        PreparedStatement pstmt = conn.prepareStatement(sql);
		        {    
		            // set parameters for statement
		            pstmt.setString(1,prenom);
		            pstmt.setString(2,nom);
		            pstmt.setString(3,pseudo);
		            pstmt.setString(4,password);
		            pstmt.setString(5,typeUtilisateur);
		            pstmt.setInt(6, id);
		            pstmt.executeUpdate();
		            }
		        }catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		      }
		}else System.out.println("L'utilisateur avec l'ID : "+id+" N'existe pas dans la base de donnees!");
		return "La modification est bien faite";
	}
//	
	@WebMethod
	public String suprimerUtilisateur(@WebParam(name="token")String token,@WebParam(name="status")String status,@WebParam(name="id")int id) {
		if(!Utilisateur.canAcces(token, status)) return "access denied";
		if(Utilisateur.userExist(id)) {
		try
	    {  
			Connection	conn = DBConnection.createConnection();
			String sql = "DELETE FROM Utilisateur WHERE id = ? ";
	        PreparedStatement pstmt = conn.prepareStatement(sql);
	        {    
	            // set parameters for statement
	            pstmt.setInt(1,id);
	            pstmt.executeUpdate();
	            }
	        }catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	      }
		}else System.out.println("L'utilisateur avec l'ID : "+id+" N'existe pas dans la base de donnees!");
		return "La supression est bien faite";
	}
}
