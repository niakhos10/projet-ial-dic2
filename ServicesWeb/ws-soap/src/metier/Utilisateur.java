package metier;
import java.util.Date;
import java.sql.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import modele.DBConnection;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
 enum TypeUtilisateur {
    admin,editeur  
}
public class Utilisateur{
	private int id;
	private String prenom;
	private String nom;
	private String pseudo;
	private String password;
	private String typeUtilisateur; 
	private Date dateDeCreation;
	private Date dateDeModification;
	
	public Utilisateur() {};
	public Utilisateur(int id,String prenom,String nom,String pseudo,String typeUtilisateur,Date dateDecreation,Date dateDeModification){
		setId(id);
		setPrenom(prenom);
		setNom(nom);
		//setPassword(password);
		setPseudo(pseudo);
		setTypeUtilisateur(typeUtilisateur);
		setDateDeCreation(dateDeCreation);
		setDateDeModification(dateDeModification);
	}
	
	public static Boolean canAcces(String token,String status) {
		try
	    {  
			Connection	conn = DBConnection.createConnection();
			String sql = "SELECT token,status FROM Token WHERE token = ?";
	        PreparedStatement pstmt = conn.prepareStatement(sql);
	        {    
	            // set parameters for statement
	            pstmt.setString(1,token);
	            ResultSet rs = pstmt.executeQuery();
	            if(rs.next()) {
	            if((rs.getString(1).equals(token)) && (rs.getString(2).equals(status))) {
	            	
	            	return true;
	            }
	         }else System.out.println("auc'un element");
	        }
	        }catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	      }
		return false;
	}
	public static Boolean userExist (int id) {
		try
	    {  
			Connection	conn = DBConnection.createConnection();
			String sql = "SELECT id FROM Utilisateur WHERE id = ?";
	        PreparedStatement pstmt = conn.prepareStatement(sql);
	        {    
	            // set parameters for statement
	            pstmt.setInt(1,id);
	            if( pstmt.executeQuery().next()) {
	            	return true;
	            }
	            }
	        }catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	      }
		return false;
	}
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTypeUtilisateur() {
		return typeUtilisateur;
	}

	public void setTypeUtilisateur(String typeUtilisateur) {
		if(typeUtilisateur.equals("admin"))
		this.typeUtilisateur = "admin";
		if(typeUtilisateur.equals("editeur"))
			this.typeUtilisateur = "editeur";
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDateDeCreation() {
		return dateDeCreation;
	}
	public void setDateDeCreation(Date dateDeCreation) {
		this.dateDeCreation = dateDeCreation;
	}
	public Date getDateDeModification() {
		return dateDeModification;
	}
	public void setDateDeModification(Date dateDeModification) {
		this.dateDeModification = dateDeModification;
	}
}
