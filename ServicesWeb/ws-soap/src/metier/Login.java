package metier;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

import modele.DBConnection;
public class Login{
	public	static String createToken(String pseudo,String password) {
			UUID uuid = UUID.randomUUID();
			String token = uuid.toString();
			try
		    {  
				Connection	conn = DBConnection.createConnection();
				String sql = "INSERT INTO Token(token,label,status)"
		                   + "VALUES(?,?,?)";
		        PreparedStatement pstmt = conn.prepareStatement(sql);
		        {    
		            // set parameters for statement
		            pstmt.setString(1,token);
		            pstmt.setString(2,pseudo+" Connection");
		            pstmt.setString(3,"actif");
		            pstmt.executeUpdate();
		            }
		        }catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		      }
	       return token;
		}

}
