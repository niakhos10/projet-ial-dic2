const config = require('./src/config/environment/index.ts').default;

module.exports = {
  ...config.db,
  logging: true,
  entities: ['src/db/entity/**/*.ts'],
  migrations: ['src/db/migration/**/*.ts'],
  cli: {
    entitiesDir: ['src/db/entity'],
    migrationsDir: 'src/db/migration',
    subscribersDir: 'src/db/subscriber',
  },
};