import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity('Article')
export class Article {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    titre: string;

    @Column()
    contenu: string;

    @Column()
    dateCreation: Date;

    @Column()
    categorie: Date;
}
