import * as bodyParser from 'body-parser';
import cors = require('cors');
import express from 'express';
import session from 'express-session'
import config from './config/environment'
import {logger} from './shared/Logs';
import morgan from 'morgan';

class App {
  app: express.Application;

  constructor() {
    this.app = express();
    this.config();
  }

  private config = () => {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({extended: false}));
    this.app.use(express.static('public'));
    this.app.use(cors());
    this.app.use(morgan('combined'));
  };
}
const application = new App();
export default application.app;
