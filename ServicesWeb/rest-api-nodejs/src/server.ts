import app from './app';
import config from './config/environment';
import registerRoutes from './routes/index';
import {createConnection} from 'typeorm';
import { logger } from './shared/Logs';
const PORT = config.port;

createConnection().then(connection => {
  registerRoutes(app);
});
let server = app.listen(PORT, () => {
  logger.info('Express server listening on port ' + PORT);
});
