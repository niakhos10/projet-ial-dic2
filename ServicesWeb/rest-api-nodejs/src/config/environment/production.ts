'use strict';
/*eslint no-process-env:0*/

// Production specific configuration
// =================================
require('dotenv').config();
export default {
  // Server port
  port: process.env.PORT || 9000,

  // Mysql connection options
  db: {
    password: '',
    username: process.env.DB_USERNAME,
    database: process.env.DB_NAME,
    logging: false,
    type: 'mysql',
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
  },
  secret: {
    session_name: process.env.SESSION_NAME
  }
};
