'use strict';
/*eslint no-process-env:0*/

const _ = require('lodash');

// All configurations will extend these options
// ============================================
const env = process.env.NODE_ENV || 'development';
const all = {
  env,
  // Server port
  port: process.env.PORT || 9000,
};

// Export the config object based on the NODE_ENV
// ==============================================
const config = _.merge(all, require(`./${env}.ts`).default || {});
export default config;
