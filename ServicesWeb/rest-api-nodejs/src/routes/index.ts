
import ArticleRouter from '../api/article/ArticleRouter'
export default function (app: any) {
    app.use('/api/article/',ArticleRouter);
}
  