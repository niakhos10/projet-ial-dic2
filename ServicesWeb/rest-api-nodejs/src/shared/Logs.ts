import winston from "winston";
import 'winston-daily-rotate-file';
import config from "../config/environment";

let { createLogger, format, transports } = winston;
const ENV = config.env;
let prodTransports = [
  new transports.DailyRotateFile({
    dirname: "logs",
    filename: "error.log",
    maxSize: "100m",
    level: "error",
    format: format.combine(
      format.timestamp({ format: "YYYY-MM-DDTHH:mm:ss.sss" })
    )
  }),
  new transports.DailyRotateFile({
    dirname: "logs",
    filename: "production.log",
    maxSize: "100m",
    format: format.combine(
      format.timestamp({ format: "YYYY-MM-DDTHH:mm:ss.sss" })
    )
  })
];
let defaultTransport = [new transports.Console()];

let loggerFormat = format.combine(
  format.timestamp({
    format: "YYYY-MM-DD HH:mm:ss"
  }),
  format.errors({ stack: true }),
  format.json()
);

export const logger = createLogger({
  level: "info",
  format: loggerFormat,
  transports: ENV === "production" ? prodTransports : defaultTransport
});
export const errorLogger = createLogger({
  level: "error",
  format: loggerFormat,
  transports: ENV === "production" ? prodTransports : defaultTransport
});;
