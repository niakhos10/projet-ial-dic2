import frError from './fr';
export function getErrorMessage(lang, messageId) {
  return frError[messageId];
}
