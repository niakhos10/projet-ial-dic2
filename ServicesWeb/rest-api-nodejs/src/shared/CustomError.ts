export class CustomError extends Error {
  static buildFromSimpleMessages(messages: string[]) {
    const formattedMessages = messages.map(message => ({message}));
    const error = new CustomError(formattedMessages);
    return error;
  }
  
  static buildIncorrectRessource(status = 'RESSOURCE_INCORRECT', message) {
    const error = new CustomError([]);
    error.errorCode = status;
    error.errorMessage = message;
    return error;
  }

  private messages: Array<{message: string; errorCode?: string}>;
  errorCode: string;
  errorMessage: string;
  constructor(messages: Array<{message: string; errorCode?: string}>) {
    super('Some errors occured.');
    this.messages = messages;
  }

  getMessages = () => {
    return this.messages;
  };
}
