import {CustomError} from './CustomError';

export function validateFullName(firstName: string, lastName: string) {
  if (!firstName || !lastName) {
    throw CustomError.buildIncorrectRessource(
      'NAME_INCORRECT',
      'Le nom et le prénom sont obligatoires.'
    );
  }

  if (firstName.length < 2) {
    throw CustomError.buildIncorrectRessource(
      'FIRST_NAME_TOO_SHORT',
      'Le prénom est trop court.'
    );
  }

  if (lastName.length < 2) {
    throw CustomError.buildIncorrectRessource(
      'LAST_NAME_TOO_SHORT',
      'Le nom est trop court'
    );
  }
  return true;
}

export function validatePassword(password: string) {
  if (password && password.trim().length >= 6) {
    return true;
  }
  throw CustomError.buildIncorrectRessource(
    'PASSWORD_TOO_SHORT',
    'Mot de passe trop court.'
  );
}
