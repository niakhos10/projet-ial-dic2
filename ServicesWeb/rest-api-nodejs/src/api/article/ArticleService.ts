import {getRepository} from 'typeorm';
import { Article}  from '../../db/entity/Article';
function getBeneficiaireRepository() {
    return getRepository(Article);
  }
  

export const articleService = {
    async getArticles(){
        return getBeneficiaireRepository().find();
      
    },
    async getArticlesByCategory(category){
        return getBeneficiaireRepository().find({
            where:{categorie:category}
        });
      
    },
    async getArticlesGroupeByCategory(){
        return getBeneficiaireRepository().createQueryBuilder('Article').select('categorie',).groupBy('categorie')
        .getRawMany();;
      
    }

}