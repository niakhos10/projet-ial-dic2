import {Request, Response} from 'express';
import httpCode from 'http-status-codes';
import {articleService} from './ArticleService'
import { logger } from '../../shared/Logs';
import {handleError, getStatusCode} from '../commun/Utils'

export async function getArticles(req: Request, res: Response) {
  let {format} = req.params;
  console.log("Format rendu",format);
    try {
      let articles = await articleService.getArticles();
      if (!articles) {
        return res.status(httpCode.BAD_REQUEST).json({
          message: 'Bad request',
          status: 'FAILED'
        })
      }
      if (format=="xml"){
        var xml2js = require('xml2js');
        var builder = new xml2js.Builder();
        var xml = builder.buildObject({
          status: 'SUCCESS',
          data: articles,
        });
        console.log(xml);
        res.writeHead(200, {"Content-Type": "text/html"});  
        res.write(xml);  
        res.end();        
      }
      
      return res.json({
        status: 'SUCCESS',
        data: articles,
      })
    } catch (error) {
      logger.error(error);
      const statusCode = getStatusCode(error.errorCode);
      return handleError(res, statusCode, error);
    }
  }



export async function getArticlesByCategory(req: Request, res: Response) {
  let {categorieId,format} = req.params;
  console.log("Format rendu",format);
    try {
      let articles = await articleService.getArticlesByCategory(categorieId);
      if (!articles) {
        return res.status(httpCode.BAD_REQUEST).json({
          message: 'Bad request',
          status: 'FAILED'
        })
      }
      if (format=="xml"){
        var xml2js = require('xml2js');
        var builder = new xml2js.Builder();
        var xml = builder.buildObject({
          status: 'SUCCESS',
          data: articles,
        });
        console.log(xml);
        res.writeHead(200, {"Content-Type": "text/html"});  
        res.write(xml);  
        res.end();        
      }
      
      return res.json({
        status: 'SUCCESS',
        data: articles,
      })
    } catch (error) {
      logger.error(error);
      const statusCode = getStatusCode(error.errorCode);
      return handleError(res, statusCode, error);
    }
  }
export async function getArticlesGroupeByCategory(req: Request, res: Response) {
  let {format} = req.params;
  console.log("Format rendu",format);
    try {
      let articles = await articleService.getArticlesGroupeByCategory();
      if (!articles) {
        return res.status(httpCode.BAD_REQUEST).json({
          message: 'Bad request',
          status: 'FAILED'
        })
      }
      if (format=="xml"){
        var xml2js = require('xml2js');
        var builder = new xml2js.Builder();
        var xml = builder.buildObject({
          status: 'SUCCESS',
          data: articles,
        });
        console.log(xml);
        res.writeHead(200, {"Content-Type": "text/html"});  
        res.write(xml);  
        res.end();        
      }
      return res.json({
        status: 'SUCCESS',
        data: articles,
      })
    } catch (error) {
      logger.error(error);
      const statusCode = getStatusCode(error.errorCode);
      return handleError(res, statusCode, error);
    }
  }