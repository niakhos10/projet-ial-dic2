import { Router } from 'express';
import {getArticles,getArticlesByCategory, getArticlesGroupeByCategory} from './ArticleController'
const router = Router();
router.get('/:format', getArticles);
router.get('/categorie/:categorieId/:format', getArticlesByCategory);
router.get('/groupe-by-category/:format', getArticlesGroupeByCategory);
export default router;