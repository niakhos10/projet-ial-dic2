import { Response } from 'express';
import httpCode from 'http-status-codes';
import { logger } from '../../shared/Logs';
import { CustomError } from '../../shared/CustomError';

export function handleError(res: Response, statusCode: number, error: CustomError) {
    const status = statusCode || 500;
    logger.error({error});
    const errorCode = error.errorCode || 'INTERNAL_ERROR';
    return res.status(status).json({
      errorCode,
      errorMessage: error.errorMessage,
      errors: error.getMessages(),
      status: 'FAILED'
    });
  }
  
export function getStatusCode(errorCode: string) {
    const options = {
      NO_AUTH: httpCode.UNAUTHORIZED,
      RESSOURCES_NOT_FOUND: httpCode.NOT_FOUND,
      SERVICE_OPTION_NOT_FOUND: httpCode.NOT_FOUND,
      USER_SHOULD_NOT_TRANSFER: httpCode.FORBIDDEN,
      AMOUNT_NOT_AUTHORIZED: httpCode.FORBIDDEN,
      DUPLICATED_COMMAND_REF: httpCode.NOT_ACCEPTABLE,
      TRANSACTION_NOT_EXIST: httpCode.NOT_ACCEPTABLE,
      INCORRECT_RESSOURCE: httpCode.BAD_REQUEST,
      SERVER_ERROR: httpCode.INTERNAL_SERVER_ERROR
    };
  
    return options[errorCode]
      ? options[errorCode]
      : httpCode.INTERNAL_SERVER_ERROR;
  }
  
