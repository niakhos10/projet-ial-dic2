import java.util.List;
import java.util.Scanner;

import ws.Utilisateur;
import ws.*;
public class Client {
	static UtilisateursService stub = new UtilisateursService_Service().getUtilisateursServicePort();
	static Scanner sc = new Scanner(System.in);
	
	static void lister(String token,String status) {
		List<Utilisateur> liste = stub.listerUtilisateurs(token, status);
		System.out.println();
		System.out.println(" La liste des utilisateur est : ");
		for(Utilisateur elem : liste){
	        System.out.print(elem.getId()+"\t"+elem.getNom()+"\t "+elem.getPrenom()+"\t "+elem.getPseudo()+"\t "+elem.getTypeUtilisateur()+"\t "+elem.getDateDeCreation()+"\t "+elem.getDateDeModification());
	        System.out.println();
		}
	}
	
	static void ajouterUser(String token,String status) {
		String prenom,nom,pseudo,password,typeUtilisateur;
		System.out.print("Entrer le prenom : ");
		prenom = sc.next();
		System.out.print("Entrer le nom : ");
		nom = sc.next();
		System.out.print("Entrer le pseudo : ");
		pseudo = sc.next();
		System.out.print("Entrer le password : ");
		password = sc.next();
		do {
		System.out.print("Entrer le typeUtilisateur : ");
		typeUtilisateur = sc.next();
		}while(!(typeUtilisateur.equals("admin") || typeUtilisateur.equals("editeur")));
		String reponse = stub.ajouterUtilisateur(prenom, nom, pseudo, password, typeUtilisateur, token, status);
		System.out.println(reponse);
	}
	
	static void modifierUser(String token,String status) {
		String prenom,nom,pseudo,password,typeUtilisateur;int id;
		System.out.print("Entrer l'id d'utilisateur a modifier : ");
		id = sc.nextInt();
		System.out.print("Entrer le prenom : ");
		prenom = sc.next();
		System.out.print("Entrer le nom : ");
		nom = sc.next();
		System.out.print("Entrer le pseudo : ");
		pseudo = sc.next();
		System.out.print("Entrer le password : ");
		password = sc.next();
		do {
		System.out.print("Entrer le typeUtilisateur : ");
		typeUtilisateur = sc.next();
		}while(!(typeUtilisateur.equals("admin") || typeUtilisateur.equals("editeur")));
		String reponse = stub.modifierUtilisateur(token, status, id, prenom, nom, pseudo, password, typeUtilisateur);
		System.out.println(reponse);
	}
	
	static void supprimerUser(String token,String status) {
		int id;
		System.out.print("Entrer l'id de l'utilisateur a supprimer : ");
		id = sc.nextInt();
		String reponse = stub.suprimerUtilisateur(token, status, id);
		System.out.println(reponse);
	}
	
	public static void main(String[] args) {
		String pseudo,password;
		System.out.println("--------------------------------------------------------------------------");
		System.out.println("         BIENVENUE SUR L'INTERFACE DE GESTION DES UTILISATEURS            ");
		System.out.println("--------------------------------------------------------------------------");
		System.out.println();
		System.out.print("PSEUDO : ");
		pseudo = sc.nextLine();
		System.out.println();
		System.out.println();
		System.out.print("PASSWORD : ");
		password = sc.nextLine();
		System.out.println();
		String token = stub.logIn(pseudo, password);
		if(token.equals("erreur")) {
		
			System.out.println("il faut etre admin pour utiliser cette interface");
		}else {
			int choix;
			do {
			System.out.println();
			System.out.println("1 --> Lister les utilisateurs");
			System.out.println("2 --> Ajouter un utilisateur");
			System.out.println("3 --> Modifier un utilisateur");
			System.out.println("4 --> Suprimer un utilisateur");
			System.out.println("0 --> Quiter");
			choix = sc.nextInt();
			switch (choix) {
				case 1 : lister(token,"actif");break;
				case 2 : ajouterUser(token,"actif");break;
				case 3 : modifierUser(token,"actif");break;
				case 4 : supprimerUser(token,"actif");break;
					}
			}while((choix>=1) && (choix<=4));
		}
	}
}