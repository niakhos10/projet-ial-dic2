
package ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ModifierUtilisateur_QNAME = new QName("http://ws/", "modifierUtilisateur");
    private final static QName _LogInResponse_QNAME = new QName("http://ws/", "logInResponse");
    private final static QName _AjouterUtilisateurResponse_QNAME = new QName("http://ws/", "ajouterUtilisateurResponse");
    private final static QName _ListerUtilisateurs_QNAME = new QName("http://ws/", "listerUtilisateurs");
    private final static QName _ModifierUtilisateurResponse_QNAME = new QName("http://ws/", "modifierUtilisateurResponse");
    private final static QName _SuprimerUtilisateur_QNAME = new QName("http://ws/", "suprimerUtilisateur");
    private final static QName _ListerUtilisateursResponse_QNAME = new QName("http://ws/", "listerUtilisateursResponse");
    private final static QName _LogIn_QNAME = new QName("http://ws/", "logIn");
    private final static QName _AjouterUtilisateur_QNAME = new QName("http://ws/", "ajouterUtilisateur");
    private final static QName _SuprimerUtilisateurResponse_QNAME = new QName("http://ws/", "suprimerUtilisateurResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ModifierUtilisateur }
     * 
     */
    public ModifierUtilisateur createModifierUtilisateur() {
        return new ModifierUtilisateur();
    }

    /**
     * Create an instance of {@link LogInResponse }
     * 
     */
    public LogInResponse createLogInResponse() {
        return new LogInResponse();
    }

    /**
     * Create an instance of {@link AjouterUtilisateurResponse }
     * 
     */
    public AjouterUtilisateurResponse createAjouterUtilisateurResponse() {
        return new AjouterUtilisateurResponse();
    }

    /**
     * Create an instance of {@link ListerUtilisateurs }
     * 
     */
    public ListerUtilisateurs createListerUtilisateurs() {
        return new ListerUtilisateurs();
    }

    /**
     * Create an instance of {@link ModifierUtilisateurResponse }
     * 
     */
    public ModifierUtilisateurResponse createModifierUtilisateurResponse() {
        return new ModifierUtilisateurResponse();
    }

    /**
     * Create an instance of {@link SuprimerUtilisateur }
     * 
     */
    public SuprimerUtilisateur createSuprimerUtilisateur() {
        return new SuprimerUtilisateur();
    }

    /**
     * Create an instance of {@link ListerUtilisateursResponse }
     * 
     */
    public ListerUtilisateursResponse createListerUtilisateursResponse() {
        return new ListerUtilisateursResponse();
    }

    /**
     * Create an instance of {@link LogIn }
     * 
     */
    public LogIn createLogIn() {
        return new LogIn();
    }

    /**
     * Create an instance of {@link AjouterUtilisateur }
     * 
     */
    public AjouterUtilisateur createAjouterUtilisateur() {
        return new AjouterUtilisateur();
    }

    /**
     * Create an instance of {@link SuprimerUtilisateurResponse }
     * 
     */
    public SuprimerUtilisateurResponse createSuprimerUtilisateurResponse() {
        return new SuprimerUtilisateurResponse();
    }

    /**
     * Create an instance of {@link Utilisateur }
     * 
     */
    public Utilisateur createUtilisateur() {
        return new Utilisateur();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifierUtilisateur }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "modifierUtilisateur")
    public JAXBElement<ModifierUtilisateur> createModifierUtilisateur(ModifierUtilisateur value) {
        return new JAXBElement<ModifierUtilisateur>(_ModifierUtilisateur_QNAME, ModifierUtilisateur.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogInResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "logInResponse")
    public JAXBElement<LogInResponse> createLogInResponse(LogInResponse value) {
        return new JAXBElement<LogInResponse>(_LogInResponse_QNAME, LogInResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AjouterUtilisateurResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "ajouterUtilisateurResponse")
    public JAXBElement<AjouterUtilisateurResponse> createAjouterUtilisateurResponse(AjouterUtilisateurResponse value) {
        return new JAXBElement<AjouterUtilisateurResponse>(_AjouterUtilisateurResponse_QNAME, AjouterUtilisateurResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListerUtilisateurs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "listerUtilisateurs")
    public JAXBElement<ListerUtilisateurs> createListerUtilisateurs(ListerUtilisateurs value) {
        return new JAXBElement<ListerUtilisateurs>(_ListerUtilisateurs_QNAME, ListerUtilisateurs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifierUtilisateurResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "modifierUtilisateurResponse")
    public JAXBElement<ModifierUtilisateurResponse> createModifierUtilisateurResponse(ModifierUtilisateurResponse value) {
        return new JAXBElement<ModifierUtilisateurResponse>(_ModifierUtilisateurResponse_QNAME, ModifierUtilisateurResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SuprimerUtilisateur }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "suprimerUtilisateur")
    public JAXBElement<SuprimerUtilisateur> createSuprimerUtilisateur(SuprimerUtilisateur value) {
        return new JAXBElement<SuprimerUtilisateur>(_SuprimerUtilisateur_QNAME, SuprimerUtilisateur.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListerUtilisateursResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "listerUtilisateursResponse")
    public JAXBElement<ListerUtilisateursResponse> createListerUtilisateursResponse(ListerUtilisateursResponse value) {
        return new JAXBElement<ListerUtilisateursResponse>(_ListerUtilisateursResponse_QNAME, ListerUtilisateursResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogIn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "logIn")
    public JAXBElement<LogIn> createLogIn(LogIn value) {
        return new JAXBElement<LogIn>(_LogIn_QNAME, LogIn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AjouterUtilisateur }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "ajouterUtilisateur")
    public JAXBElement<AjouterUtilisateur> createAjouterUtilisateur(AjouterUtilisateur value) {
        return new JAXBElement<AjouterUtilisateur>(_AjouterUtilisateur_QNAME, AjouterUtilisateur.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SuprimerUtilisateurResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "suprimerUtilisateurResponse")
    public JAXBElement<SuprimerUtilisateurResponse> createSuprimerUtilisateurResponse(SuprimerUtilisateurResponse value) {
        return new JAXBElement<SuprimerUtilisateurResponse>(_SuprimerUtilisateurResponse_QNAME, SuprimerUtilisateurResponse.class, null, value);
    }

}
