<?php
	require_once 'ConnexionManager.php';
	require_once 'ArticleDao.php';
	/**
	 * Classe de gestion des accès aux articles
	 */
	class CategorieDao
	{
		private $id;
		private $bdd;
		private $libelle;

		public function __construct()
		{
			$this->bdd = (new ConnexionManager)->getInstance();
		}

		public function add($libelle){
			$response = $this->bdd->query('INSERT INTO Categorie (libelle) VALUES ("'.$libelle.'")');
			return $this->getById($this->bdd->lastInsertId());
		}
		public function update($id,$newLibelle){
			$response = $this->bdd->query('UPDATE Categorie SET libelle="'.$newLibelle.'" WHERE id='.intval($id));
			return $this->getById(intval($id));
		}
		public function delete($id) {
			$article = new ArticleDao();
			$article->deleteByCategoryId($id);
			if ($this->getById($id)){
				$this->bdd->query('DELETE FROM Categorie WHERE id='.intval($id));
				return true;
			}
			return false;
		}
		public function getList()
		{
			$reponse = $this->bdd->query('SELECT * FROM Categorie');
			$data = $reponse->fetchAll(PDO::FETCH_CLASS, 'Categorie');
			return $data;
		}

		public function getById($id)
		{
			$reponse = $this->bdd->query('SELECT * FROM Categorie WHERE id = '.$id);
			$data = $reponse->fetch(PDO::FETCH_OBJ);
			if (empty($data)){
				return false;
			}
			return $data;
		}
	}
?>