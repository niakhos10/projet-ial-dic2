<?php
	require_once 'ConnexionManager.php';

	/**
	 * Classe de gestion des accès aux utilisateurs
	 */
	class UtilisateurDao
	{
		private $bdd;

		public function __construct()
		{
			$this->bdd = (new ConnexionManager)->getInstance();
		}

		public function getList()
		{
			$data = $this->bdd->query('SELECT id,pseudo,prenom,nom,typeUtilisateur,dateCreation,dateModification FROM Utilisateur');
			return $data->fetchAll(PDO::FETCH_CLASS, 'Utilisateur');
		}
		public function userExist($pseudo,$password){
			$data = $this->bdd->query('SELECT id,pseudo,prenom,nom,typeUtilisateur,dateCreation,dateModification FROM Utilisateur WHERE pseudo="'.$pseudo.'" AND password="'.$password.'"');
			if($data){
				return $data->fetch(PDO::FETCH_OBJ);
			}
			return false;
		}
		public function delete($id) {
			$this->bdd->query('DELETE FROM Utilisateur WHERE id='.intval($id));
			return true;
		}
		public function getById($id)
		{
			$data = $this->bdd->query('SELECT * FROM Utilisateur WHERE id = '.$id);
			return $data->fetch(PDO::FETCH_OBJ);
		}
	}
?>