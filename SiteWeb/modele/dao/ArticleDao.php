<?php
	require_once 'ConnexionManager.php';

	/**
	 * Classe de gestion des accès aux articles
	 */
	class ArticleDao
	{
		private $bdd;

		public function __construct()
		{
			$this->bdd = (new ConnexionManager)->getInstance();
		}

		public function getList()
		{
			$data = $this->bdd->query('SELECT * FROM Article ORDER BY dateCreation DESC');
			return $data->fetchAll(PDO::FETCH_CLASS, 'Article');
		}

		public function getById($id)
		{
			$data = $this->bdd->query('SELECT * FROM Article WHERE id = '.$id);
			return $data->fetch(PDO::FETCH_OBJ);
		}
		public function add($titre,$contenu,$categorie,$utilisateurCreateurId) {
			$categorie = (int)$categorie;
			$utilisateurCreateurId = (int)$utilisateurCreateurId;
			$date = date("Y-m-d H:m:d");
			$result = $this->bdd->query('INSERT INTO Article (titre,contenu,categorie,utilisateurCreateurId) VALUES ("'.$titre.'","'.$contenu.'",'.$categorie.','.$utilisateurCreateurId.')') ;
			return $this->getById($this->bdd->lastInsertId());
		}
		public function update($id,$titre,$contenu,$categorie,$utilisateurCreateurId) {
			$categorie = intval($categorie);
			$utilisateurCreateurId = intval($utilisateurCreateurId);
			$id = intval($id);
			return $this->bdd->query('UPDATE Article  SET titre="'.$titre.'",contenu="'.$contenu.'",categorie='.$categorie.',utilisateurCreateurId='.$utilisateurCreateurId.' WHERE id='.$id);
			
		}
		public function delete($id) {
			$this->bdd->query('DELETE FROM Article WHERE id='.intval($id));
			return true;
		}
		public function getByCategoryId($id)
		{
			$data = $this->bdd->query('SELECT * FROM Article  WHERE categorie = '.$id.' ORDER BY dateCreation DESC');
			if (!$data || empty($data)){
				return false;
			}
			return $data->fetchAll(PDO::FETCH_CLASS, 'Article');
		}
		public function deleteByCategoryId($id)
		{
			if($this->getByCategoryId($id)){
				$data = $this->bdd->query('DELETE FROM Article WHERE categorie='.intval($id));
			}
			return true;
			
		}
	}
?>