<?php
	require_once 'ConnexionManager.php';

	/**
	 * Classe de gestion des accès aux tokens
	 */
	class TokenDao
	{
		private $bdd;

		public function __construct()
		{
			$this->bdd = (new ConnexionManager)->getInstance();
		}

		public function getList()
		{
			$data = $this->bdd->query('SELECT * FROM Token');
			return $data->fetchAll(PDO::FETCH_CLASS, 'Token');
		}
		public function add($libelle){
			$generateToken = sha1(date("YmdHms").uniqid());
			$response = $this->bdd->query('INSERT INTO Token (token,label) VALUES ("'.$generateToken.'","'.$libelle.'")');
			return $this->getById($this->bdd->lastInsertId());
		}
		public function delete($id) {
			$this->bdd->query('DELETE FROM Token WHERE id='.intval($id));
			return true;
		}
		public function getById($id)
		{
			$data = $this->bdd->query('SELECT * FROM Token WHERE id = '.$id);
			return $data->fetch(PDO::FETCH_OBJ);
		}

	}
?>