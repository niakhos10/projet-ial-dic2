<?php
	/**
	 * Classe métier représentant un article
	 */
	class Article
	{
		public $id;
		public $titre;
		public $image;
		public $contenu;
		public $categorie;
		public $dateCreation;
		public $dateModification;
		public $utilisateurCreateurId;

		function __construct()
		{
			
		}
	}
?>