<?php
	require_once 'modele/dao/ArticleDao.php';
	require_once 'modele/dao/CategorieDao.php';
	require_once 'modele/dao/UtilisateurDao.php';
	require_once 'modele/dao/TokenDao.php';
	require_once 'modele/domaine/Article.php';
	require_once 'modele/domaine/Categorie.php';
	require_once 'modele/domaine/Utilisateur.php';
	require_once 'modele/domaine/Token.php';

	/**
	 * Classe représentant notre controleur principal
	 */
	class Controller
	{
		private $showEditeurMenu = false;
		private $showAdminMenu = false;
		private $isUser = false;
		private $userSession = null;
		private $messageInfos = null; 
		private $pseudo = null;
		private $categorie = null;
		function __construct()
		{
			$this->loadControler();	
		}
		public function showUsers(){
			
			$utilisateurDao = new UtilisateurDao();
			$showEditeurMenu = $this->showEditeurMenu;
			$showAdminMenu = $this->showAdminMenu;
			$isUser = $this->isUser;
			$userSession = $this->userSession;
			$messageInfos = $this->messageInfos; 
			$pseudo = $this->pseudo;
			$utilisateurs = $utilisateurDao->getList();
			if(!$this->showAdminMenu){
				require_once 'vue/accessDenied.php';
				return;
			}
			require_once 'vue/utilisateur.php';
		}
		public function showTokens(){
			
			$tokenDao = new TokenDao();
			$showEditeurMenu = $this->showEditeurMenu;
			$showAdminMenu = $this->showAdminMenu;
			$isUser = $this->isUser;
			$userSession = $this->userSession;
			$messageInfos = $this->messageInfos; 
			$pseudo = $this->pseudo;
			$tokens = $tokenDao->getList();
			if(!$this->showAdminMenu){
				require_once 'vue/accessDenied.php';
				return;
			}
			require_once 'vue/token.php';
		}
		public function showAccueil()
		{
			$articleDao = new ArticleDao();
			$categorieDao = new CategorieDao();
			$this->checkCurrentCategorie($categorieDao);
			$showEditeurMenu = $this->showEditeurMenu;
			$showAdminMenu = $this->showAdminMenu;
			$isUser = $this->isUser;
			$userSession = $this->userSession;
			$messageInfos = $this->messageInfos; 
			$pseudo = $this->pseudo;
			$articles = $articleDao->getList();
			$categories = $categorieDao->getList();
			$categorie = $this->categorie;
			require_once 'vue/accueil.php';
		}
		
		public function showArticle($id)
		{
			$articleDao = new ArticleDao();
			$categorieDao = new CategorieDao();
			$this->checkCurrentCategorie($categorieDao);
			$categorie = $this->categorie;
			
			$showEditeurMenu = $this->showEditeurMenu;
			$showAdminMenu = $this->showAdminMenu;
			$isUser = $this->isUser;
			$userSession = $this->userSession;
			$messageInfos = $this->messageInfos; 
			$pseudo = $this->pseudo;
			$article = $articleDao->getById($id);
			$categories = $categorieDao->getList();
			require_once 'vue/article.php';
		}

		public function showCategorie($id)
		{
			$articleDao = new ArticleDao();
			$categorieDao = new CategorieDao();
			$this->checkCurrentCategorie($categorieDao);
			$categorie = $this->categorie;

			$showEditeurMenu = $this->showEditeurMenu;
			$showAdminMenu = $this->showAdminMenu;
			$isUser = $this->isUser;
			$userSession = $this->userSession;
			$messageInfos = $this->messageInfos; 
			$pseudo = $this->pseudo;
			$articles = $articleDao->getByCategoryId($id);
			$categories = $categorieDao->getList();
			require_once 'vue/articleByCategorie.php';
		}
		private function loadControler() {
			$this->verifyLog();
			if(isset($_SESSION["UTILISATEUR"])){
				$this->isUser = true;
				$userSession = $_SESSION["UTILISATEUR"];
				$this->pseudo = $userSession->pseudo;
				$typeUtilisateur = $userSession->typeUtilisateur;
				if($typeUtilisateur =="admin"){
					$this->showAdminMenu = true;
				} else if($typeUtilisateur == "editeur"){
					$this->showEditeurMenu = true;
				}
				if(!$this->verifyOrAddOrUpdateCategorie()){
					if(!$this->verifyOrAddOrUpdateArticle()){
						if(!$this->verifyOrAddToken()){
							$this->verifyOrAddUser();
						}
					}
				}
			}
			
		}
		private function verifyOrAddToken(){
			if (!$this->showAdminMenu) {
				return false;
			}
			if(isset($_REQUEST["addToken"])){
				if(isset($_REQUEST["libele"])){
					$libele = $_REQUEST["libele"];
					$token = new TokenDao();
					if ($result = $token->add($libele)) {
						$this->setMessageInfo("Token créé avec succès","success");
					} else {
						$this->setMessageInfo("Echec de la créattion du token","error");
					}
				}
				return true;
			} else if (isset($_REQUEST["delete_token"])){
				if(isset($_REQUEST["token_id"])) {
					$tokenId = $_REQUEST["token_id"];
					$token = new TokenDao();
					if($token->delete($tokenId)) {
						$this->setMessageInfo("Token supprimé avec succès","success");
					} else {
						$this->setMessageInfo("Echec de la suppression du token","error");

					}
				}
				return true;
			}
			return false;
			
		}
		private function verifyOrAddUser() {
			if (!$this->showAdminMenu){
				return false;
			}
			if(isset($_REQUEST["deleteUser"])){
				if(isset($_REQUEST["userId"])){
					$userId = $_REQUEST["userId"];
					$utilisateur = new UtilisateurDao();
					if($utilisateur->delete($userId)){
						$this->setMessageInfo("Utilisateur supprimé avec succès","success");
					} else {
						$this->setMessageInfo("Echec de la suppression de l'utilisateur","error");
					}
				}
				return true;
			}
			return false;
		}
		private function verifyLog(){
			if(isset($_REQUEST["login"])){
				if($_REQUEST["pseudo"] && isset($_REQUEST["password"])){
					$pseudo = $_REQUEST["pseudo"];
					$password = $_REQUEST["password"];
					$utilisateur = new UtilisateurDao();
					if($result = $utilisateur->userExist($pseudo,$password)){
						$_SESSION["UTILISATEUR"] = (object)[
							"id" => $result->id,
							"typeUtilisateur" => $result->typeUtilisateur,
							"prenom" => $result->prenom,
							"nom" => $result->nom,
							"pseudo" => $result->pseudo,
						];
						$this->setMessageInfo("Connexion réussie","success");
					} else {
						$this->setMessageInfo("Pseudo ou mot de passe incorrect","error");
					}
				} else {
					$this->setMessageInfo("Le pseudo et le mot de passe sont obligatoire","error");
				}
			}
		}
		private function verifyOrAddOrUpdateCategorie(){
			if (isset($_REQUEST["categorie_add"])){
				if (isset($_REQUEST["libele"])){
					$libelle = $_REQUEST["libele"];
					$categorieDao = new CategorieDao();
					if ($categorieDao->add($libelle)){
						$this->setMessageInfo("Ajout catégorie réussie","success");
					} else{
						$this->setMessageInfo("Echec de l'ajout de la catégorie","error");
					}
				} else {
					$this->setMessageInfo("Le libellé est obligatoir","error");
				}
				return true;
			}
			if(isset($_REQUEST["categorie_set"])) {
				if (isset($_REQUEST["id"]) && isset($_REQUEST["libele"])){
					$id = $_REQUEST["id"];
					$newLibelle = $_REQUEST["libele"];
					$categorieDao = new CategorieDao();
					if ($categorieDao->update($id,$newLibelle)){
						$this->setMessageInfo("Mis à jours réussie","success");
					} else{
						$this->setMessageInfo("Echec de la mis à jour","error");
					}
				}
				return true;
			}
			if (isset($_REQUEST["categorie_delete"])) {
				if (isset($_REQUEST["id"])){
					$id = $_REQUEST["id"];
					$categorieDao = new CategorieDao();
					if($categorieDao->getById((int)$id)){
						$categorieDao->delete($id);
						$this->setMessageInfo("Catégorie supprimée avec succès","success");
					}else{
						$this->setMessageInfo("Echec de la suppression","error");
					}
				}
				return true;
			}
			return false;
		}
		private function verifyOrAddOrUpdateArticle() {
			if (isset($_REQUEST["article_add"])){
				if (isset($_REQUEST["titre"]) && isset($_REQUEST["contenu"]) && isset($_REQUEST["categorie"])){
					$titre = $_REQUEST["titre"];
					$contenu = $_REQUEST["contenu"];
					$categorie = $_REQUEST["categorie"];
					$userId = $_SESSION["UTILISATEUR"]->id;

					$articleDao = new ArticleDao();
					if ($articleDao->add($titre,$contenu,$categorie,$userId)){
						$this->setMessageInfo("Article ajouté avec succès","success");
					} else{
						$this->setMessageInfo("Echec de l'ajout de l'article","error");
					}
				} else {
					$this->setMessageInfo("Le titre, le contenu et la catégorie sont obligatoires","error");
				}
				return true;
			}
			if(isset($_REQUEST["article_set"])) {
				if (isset($_REQUEST["titre"]) && isset($_REQUEST["contenu"]) && isset($_REQUEST["categorie"])){
					$articleId = $_REQUEST["id"];
					$titre = $_REQUEST["titre"];
					$contenu = $_REQUEST["contenu"];
					$categorie = $_REQUEST["categorie"];
					$userId = $_SESSION["UTILISATEUR"]->id;
					$articleDao = new ArticleDao();

					if ($articleDao->update($articleId,$titre,$contenu,$categorie,$userId)){
						$this->setMessageInfo("Article modifié avec succès","success");
					} else{
						$this->setMessageInfo("Echec de l'ajout de l'article","error");
					}
				} else {
					$this->setMessageInfo("Le titre, le contenu et la catégorie sont obligatoires","error");
				}
				return true;
			}
			if (isset($_REQUEST["article_delete"])) {
				if (isset($_REQUEST["id"])){
					$id = $_REQUEST["id"];
					$articleDao = new ArticleDao();
					if($articleDao->getById((int)$id)){
						$articleDao->delete($id);
						$this->setMessageInfo("Article supprimée avec succès","success");
					}else{
						$this->setMessageInfo("Echec de la suppression de l'article","error");
					}
				}
				return true;
			}
			return false;
		}
		private function checkCurrentCategorie($categorieDao){
			if(isset($_REQUEST["action"],$_REQUEST["id"])){
				$id = $_REQUEST["id"];
				if($_REQUEST["action"]=="categorie"){
					$this->categorie = $categorieDao->getById($id);
				}
			}
		}
		public function  ShowErrorPage(){
			require_once("vue/pageError.php");
		}
		private function setMessageInfo($msg,$status){
			$this->messageInfos = (object)[
				"message"=>$msg,
				"status"=>$status
			];
		}
	}
?>
