<?php 
	require_once('body/head.php');
	printHead("Affichage d'un article");
	require_once 'inc/entete.php'; 
	require_once 'inc/menu.php';
	if (!empty($article)): 
		 ?>
		<div  id="articleContener">
			<h1><?= $article->titre ?> 
				<?php 
				if($showEditeurMenu || $showAdminMenu){ ?>
					<span  data-toggle="modal" data-target="#articleModal_set" class="btn btn-info">Éditer</span> <?php 
				} ?>
			</h1>
			<span>Publié le <?= $article->dateCreation ?></span>
			<p><?= $article->contenu ?></p>
		</div>
	<?php else: ?>
		<div  id="articleContener" class="message">L'article demandé n'existe pas</div>
	<?php endif; 
	
	require_once("body/footer.php"); 
?>
