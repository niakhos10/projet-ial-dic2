<?php 
	require_once('body/head.php');
	printHead("Utilisateurs");
	require_once 'inc/entete.php'; 
	?>
	<div id="users">
        <h3 style="text-align:center">La liste des utilisateurs</h3><br/>
        <?php if (!empty($utilisateurs)): ?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#ID</th>
                    <th>Pseudo</th>
                    <th>Prenom</th>
                    <th>Nom</th>
                    <th>Type de compte</th>
                    <th>Date de création</th>
                    <th>Date de dernière modification</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>

                    <?php foreach ($utilisateurs as $utilisateur): ?>
                        <tr>
                           <td><?= $utilisateur->id ?></td> 
                           <td><?= $utilisateur->pseudo ?></td> 
                           <td><?= $utilisateur->prenom ?></td> 
                           <td><?= $utilisateur->nom ?></td> 
                           <td><?= $utilisateur->typeUtilisateur ?></td> 
                           <td><?= $utilisateur->dateCreation ?></td> 
                           <td><?= $utilisateur->dateModification ?></td> 
                           <td>
                               <button  data-toggle="modal" data-target="#utilisateurModal_add" class="btn btn-info">Modifier</button>
                               <a href="index.php?action=listeUtilisateurs&deleteUser=true&userId=<?= $utilisateur->id ?>"><button class="btn btn-danger">Supprimer</button></a>
                           </td> 
                        </tr>
                        <?php endforeach ?>
                </tbody>
            </table>
		<?php endif ?>
	</div>
	<?php 
	
	require_once("body/footer.php"); 
?>
