<?php 
if(!$isUser){
	getLoginModal();
}else{
	getCategorieModal();
	if(isset($categories)){
		getArticleModal($categories);
	}
	if(isset($categorie) && $categorie){
		getCategorieModal("set",$categorie->id,$categorie->libelle);
	}
	if(isset($article) && !empty($article)) {
		getArticleModal($categories,"set",$article->id,$article->categorie,$article->titre,$article->contenu);
	}
	if ($showAdminMenu) {
		ajouterUtilisateurModal();
		ajouterUtilisateurModal("add");
		creerToken();
	}
}

?>
<div style="position: fixed;top:0px;width:100%;background-color:#8d744b;z-index: 1000;color:white">
	<div class="container">
		<div style="padding-top:10px" class="row">
			<div  class="col-lg-4">
				<input style="border-radius:30px" class="form-control" placeholder="Rechercher..." type="text">
			</div>
			<div class="col-lg-6"> 
				<a href="index.php" style="color: white;">
				<table>
					<tr>
						<td><img style="width:40px" src="./assets/images/logo.png" alt=""></td>
						<td><h3 style="position:absolute;margin-top:-18px">Site d'actualité de l'ESP</h3></td>
					</tr>
				</table> </a>
			</div>
			<div class="col-lg-2">
				<?php 
				if($isUser){ ?>
					<a href="logout.php">
						<div style="position: absolute;margin-top:10px;cursor:pointer">
							<span style="border:2px solid white;border-radius:100%;padding:5px"><i class="far fa-user"></i></span>	Se déconnecter
						</div> 
					</a>
					<?php
				} else {
					?>
					<div style="position: absolute;margin-top:10px;cursor:pointer"  data-toggle="modal" data-target="#myModal">
						<span style="border:2px solid white;border-radius:100%;padding:5px"><i class="far fa-user"></i></span>	Se connecter
					</div>	
					<?php
				}
				?>
				
			</div>
		</div>
	</div>
	<hr>
	<?php 
	if($showAdminMenu){
		?>
		<div class="container user-menu">
			<ul>
				<li><a href="#" data-toggle="modal" data-target="#utilisateurModal_add">Ajouter un utilisateur</a></li>
				<li><a href="index.php?action=listeUtilisateurs">Liste des utilisateurs</a></li>
				<li><a href="#" data-toggle="modal" data-target="#tokenModal_add">Créer un jeton</a></li>
				<li><a href="index.php?action=listeJetons">Liste des jetons</a></li>
			</ul>
		</div>
		<?php
	}
	?>
</div>
<div style="height: 100%;
	background: url('./assets/images/bg3.png');
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;;position:fixed;height:100%;width:100%;overflow-y:scroll;">
<div style="position:relative;margin-top:150px;background-color:white" class="container">
<div style="background-color:white;width:60%;min-height:720px;position:fixed;overflow-y:auto;padding-bottom:20px">
<?php
if(($showAdminMenu || $showEditeurMenu) && isset($articles)){
	?>
	<div class="container">
		<ul>
			<li><a href="#"  data-toggle="modal" data-target="#articleModal_add">Ajouter un article</a> </li>
	
			<?php 
			if(isset($categorie) && $categorie){
				?>
				<li><a href="#"   data-toggle="modal" data-target="#categorieModal_set">Modifier la catégorie <?= $categorie->libelle ?></a>
			</li>
				<?php
			} ?>
		</ul>
	</div>
	<?php
}
?>
