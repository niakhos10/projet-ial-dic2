<?php
function getLoginModal(){ ?>
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Connexion</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <form action="#" method="post">

                <!-- Modal body -->
                <div class="modal-body">
                    <input type="text" class="form-control" placeholder="Pseudo" required name="pseudo"><br/><br/>
                    <input type="password" class="form-control" placeholder="Password" required name="password">
                    
                    </div>
                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <input type="submit" class="btn-success" name="login" value="Se connecter" />
                    </div>
                </form>

            </div>
        </div>
    </div>
    <?php
}
function getCategorieModal($operation="add",$id=null,$libele=null){ ?>
    <div class="modal" id="categorieModal_<?= $operation ?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <?php 
                if($id){
                    ?><input type="hidden" name="id" value="<?php echo $id; ?>"><?php
                }
                ?>
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title"><?php 
                        if($operation=="add"){ 
                            echo "Ajouter une catégorie";
                        }else if($operation=="set"){ 
                            echo "Modifier une categorie";
                        } ?>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <form action="#" method="post">

                <!-- Modal body -->
                <div class="modal-body">
                    <input type="text" class="form-control" placeholder="Libelé de la catégorie" value="<?= $libele ?>" required name="libele"><br/>
                    
                </div>
                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <?php 
                            if($operation=="set"){ ?>
                                <input type="submit" class="btn btn-danger" name="categorie_delete" value="Supprimer" /><?php 
                            } ?>
                        <input type="submit" class="btn btn-success" name="categorie_<?= $operation ?>" value="<?= ($operation=="add")?'Ajouter':'Modifier' ?>" />
                    </div>
                </form>

            </div>
        </div>
    </div> 
    <?php
}

function creerToken(){ ?>
    <div class="modal" id="tokenModal_add">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Créer un token</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <form action="#" method="post">

                <!-- Modal body -->
                <div class="modal-body">
                    <input type="text" class="form-control" placeholder="Libellé du token" required name="libele"><br/>
                    
                </div>
                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success" name="addToken" value="Créer" />
                    </div>
                </form>

            </div>
        </div>
    </div> 
    <?php
}


function getArticleModal($categories,$operation="add",$id=null,$categorie=null,$titre=null,$contenu=null){ ?>
    <div class="modal" id="articleModal_<?= $operation ?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <?php 
                    if($id){
                        ?><input type="hidden" name="id" value="<?php echo $id; ?>"><?php
                    }
                ?>
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title"><?php if($operation=="add"){ echo "Ajouter un article";}else if($operation=="set"){ echo "Modifier un article";} ?></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <form action="#" method="post">

                <!-- Modal body -->
                <div class="modal-body">
                    <select name="categorie" style="width:100%" class="form-control select2" >
                        <?php 
                        foreach ($categories as $categorie) {
                            $selected="";
                            if ($categorie->categorie==$categorie){
                                $selected = "selected";
                            }
                            ?>
                            <option <?= $selected ?> value="<?= $categorie->id ?>"><?= $categorie->libelle ?></option>
                            <?php
                        }
                        ?>
                    </select><br/><br/>
                    <input type="text" class="form-control" value="<?= $titre ?>" placeholder="Titre" required name="titre"><br/><br/>
                    <textarea class="form-control" name="contenu" placeholder="Contenu" rows="3"><?= $contenu ?></textarea>
                    
                    </div>
                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                    <?php 
                            if($operation=="set"){ ?>
                                <input type="submit" class="btn btn-danger" name="article_delete" value="Supprimer" /><?php 
                            } ?>
                        <input type="submit" class="btn btn-success" name="article_<?= $operation ?>" value="<?= ($operation=="add")?'Ajouter':'Modifier' ?>" />
                    
                    </div>
                </form>

            </div>
        </div>
    </div> 
    <?php
}



function ajouterUtilisateurModal($operation="add",$id=null,$prenom=null,$nom=null,$pseudo=null,$password=null){ ?>
    <div class="modal" id="utilisateurModal_<?= $operation ?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <?php 
                    if($id){
                        ?><input type="hidden" name="id" value="<?php echo $id; ?>"><?php
                    }
                ?>
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title"><?php if($operation=="add"){ echo "Ajouter un utilisateur";}else if($operation=="set"){ echo "Modifier un utilisateur";} ?></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <form action="#" method="post">

                <!-- Modal body -->
                <div class="modal-body">
                   
                    <input type="text" class="form-control" value="<?= $prenom ?>" placeholder="Prénom" required name="prenom"><br/><br/>
                    <input type="text" class="form-control" value="<?= $nom ?>" placeholder="Nom" required name="nom"><br/><br/>
                    <input type="text" class="form-control" value="<?= $pseudo ?>" placeholder="Pseudo" required name="pseudo"><br/><br/>
                    <input type="password" class="form-control" value="<?= $password ?>" placeholder="Password" required name="password"><br/><br/>
                    <select name="typeUtilisateur" class="form-control">
                        <option value="admin">Admin</option>
                        <option value="editeur">Editeur</option>
                    </select>
                    
                    </div>
                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                    <?php 
                            if($operation=="set"){ ?>
                                <input type="submit" class="btn btn-danger" name="utilisateur_delete" value="Supprimer" /><?php 
                            } ?>
                        <input type="submit" class="btn btn-success" name="utilisateur_<?= $operation ?>" value="<?= ($operation=="add")?'Ajouter':'Modifier' ?>" />
                    
                    </div>
                </form>

            </div>
        </div>
    </div> 
    <?php
}


?>