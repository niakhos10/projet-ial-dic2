<?php 
	require_once('body/head.php');
	printHead("Utilisateurs");
	require_once 'inc/entete.php'; 
	?>
	<div id="users">
        <h3 style="text-align:center">La liste Jetons</h3><br/>
        <?php if (!empty($tokens)): ?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#ID</th>
                    <th>token</th>
                    <th>label</th>
                    <th>Status</th>
                    <th>Date de création</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>

                    <?php foreach ($tokens as $token): ?>
                        <tr>
                           <td><?= $token->id ?></td> 
                           <td><?= $token->token ?></td> 
                           <td><?= $token->label ?></td> 
                           <td><?= $token->status ?></td> 
                           <td><?= $token->dateCreation ?></td> 
                           <td>
                               <a href="index.php?action=listeJetons&delete_token=true&token_id=<?= $token->id ?>"  class="btn btn-danger">Supprimer</a>
                           </td> 
                        </tr>
                        <?php endforeach ?>
                </tbody>
            </table>
            <?php else: ?>
			<div  id="users" class="message">Aucun token trouvé</div>
		<?php endif ?>
	</div>
	<?php 
	
	require_once("body/footer.php"); 
?>
