    </div></div>
    </div>

    <script src="https://kit.fontawesome.com/a48e396b54.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js" integrity="sha512-WNLxfP/8cVYL9sj8Jnp6et0BkubLP31jhTG9vhL/F5uEZmg5wEzKoXp1kJslzPQWwPT1eyMiSxlKCgzHLOTOTQ==" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="./assets/js/notify.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="./assets/js/script.js"></script>

    <?php 
    if(isset($messageInfos) && $messageInfos){
        ?>
        <script>
            notif("<?= $messageInfos->message ?>","<?= $messageInfos->status ?>");
        </script>
        <?php
    }
    ?>
</body>
</html>